package com.appsdeveloperblog.app.ws.service;

import com.appsdeveloperblog.app.ws.shared.dto.UserDto;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {
    UserDto createdUser(UserDto user);

    UserDto getUser(String email);

    UserDto getUserByUserId(String id);

    UserDto updatedUser(String id, UserDto userDto);

    void deletedUser(String id);

    List<UserDto> getUsers(int page, int limit);

    boolean verifyEmailToken(String token);
}
