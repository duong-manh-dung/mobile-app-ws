package com.appsdeveloperblog.app.ws.ui.model.response;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
public class ErrorMessage {
    private Date date;
    private String errorMessage;

    public ErrorMessage(Date date, String errorMessage) {
        this.date = date;
        this.errorMessage = errorMessage;
    }
}
